# Automated Guitar: Fretting module


## 3D printed Fretting module

<p float="left">
  <img src="../../Documents/pictures and video/fretting module.png" height="300" />
</p>

You will need to print the following parts in order to build the Fretting module.

1) Fretting module frame base [STL](Fretting module frame base.stl)
2) Fretting module frame [STL](Fretting module frame.stl)


Most printers should be able to print the parts, with all parts being able to fit on a 150mmx140mm build plate.

On an Ultimaker 2+, we achieved good results with the following settings:

- layer height: 0.2mm
- wall thickness: 1.5mm
- infill density: 20%
- infill pattern: grid
- print speed 80 mm/s
- supports where required for the Fretting module frame and muting module.

parts have been printed using PLA, ABS and PETG all with good results. the print was not affected very much by the print settings. However,  printing slower and with smaller layer height will improve the print quality. Tree supports where found to be easer to remove from prints in post prosessing although normal supports can be used insted. 


## Bill of materials
The robot Guitar relies on readily available hobby electronics. The components can be changed for other makes or types but changing the components will have an effect on how each module operates, for some types of guitar changes may be needed to allow the modules to operate as desired. links have been provided for UK suppliers for the components used to create the module, these suppliers are not the only source of the components and they can be bought from other suppliers or locations. 
   You will need the following components:

### General

- 1x Arduino mega was used for this project although any Arduino can be used [UK](https://www.amazon.co.uk/ELEGOO-Controller-ATmega2560-ATMEGA16U2-Compatible/dp/B06XKMZ3T9/ref=sr_1_1_sspa?dchild=1&keywords=Arduino+mega&qid=1614255721&sr=8-1-spons&psc=1&spLa=ZW5jcnlwdGVkUXVhbGlmaWVyPUEzM1FEMENJVzdIVEk4JmVuY3J5cHRlZElkPUEwNTU1OTQ0MjQ4N0QxTTZZNFdDUCZlbmNyeXB0ZWRBZElkPUEwMzI4MTAzMTlYOUw0TUpKUThYMiZ3aWRnZXROYW1lPXNwX2F0ZiZhY3Rpb249Y2xpY2tSZWRpcmVjdCZkb05vdExvZ0NsaWNrPXRydWU=)
- 12V battery is needed to power the modules [UK](https://www.amazon.co.uk/SUNPADOW-Battery-Airplane-Quadcopter-Helicopter/dp/B08Q79M7QB/ref=sr_1_2?dchild=1&keywords=12V+LIPO&qid=1614255912&sr=8-2)

- Up to 6 JF-0530B solenoids per module [UK](https://www.amazon.co.uk/Rtengtunn-JF-0530B-Push-Pull-Gangbei-0530B-Electromagnet/dp/B08291L2XL/ref=sr_1_7?dchild=1&keywords=JF-0530B&qid=1614263171&sr=8-7), for playing cowboy chords 12 solenoids
in total are needed: 5 for fret 3, 4 for fret 2 and 3 for fret 1.

- The 3-D Printed body

- 2 Printed legs for each module, although not all the modules need legs for
support. When using three modules only the first and last module needed legs.

- optional non-slip material such as foam pad can be added to the inside of the leg to add grip.

- M3 nut and bolt per leg is required to tension the leg and keep it attached to
the body [UK](https://www.amazon.co.uk/Screw-Bolts-Stainless-Steel-340pcs/dp/B08RRW6B3H/ref=sr_1_12?dchild=1&keywords=M3+nut+and+bolt&qid=1614263398&sr=8-12)

- The following are needed per-solenoids used
- 3-D printed fingertip
- TIP120 Darlington Transistor [UK](https://www.amazon.co.uk/BOJACK-Epitaxial-Transistor-Darlington-Transistors/dp/B08D8SJPCG/ref=sr_1_4?dchild=1&keywords=TIP120+Darlington+Transistor&qid=1614263478&sr=8-4)
- 1K Ohm Resistor [UK](https://www.amazon.co.uk/sourcing-map-Metal-Resistors-Tolerances/dp/B07LGM23Y4/ref=sr_1_10?dchild=1&keywords=1K+Ohm+Resistor&qid=1614263525&sr=8-10)
- 1N4001 Diode [UK](https://www.amazon.co.uk/ExcLent-100Pcs-1N4001-50V-Diode/dp/B07J3ZT55G/ref=sr_1_8?dchild=1&keywords=1N4001+Diode&qid=1614263550&sr=8-8)


### Build instructions

#### Fretting module
<p float="left">
  <img src="../../Documents/pictures and video/single solenoid wiring diagram.PNG" height="300" />
</p>

1. Print out mounting frame, mounting plate and six strumming arms and remove
supports and any other stray parts from the printing process
2. Insert a solenoid nut first into the bottom half of the 3D-printed body, the
bottom of the holding box has the nubs on each side of it, repeat this for each
location where a solenoid is required for fret fingering. Make sure the wires are
free to move around the top of the box
3. Push the wires through the holes in the lid of the holding box and attach the
lid, making sure that the solenoid plunger can freely move up and down. If the
plunger does not move freely and gets stuck carefully pull the wires to tighten
them and rotate the plunger until it moves freely.
4. connected solenoid circuit following the wiring diagram 
  -   connect one solenoid contact to the common power rail.
  -   connect the other solenoid contacts to the middle pin (pin C) of the darlington transistor.
  -   connect the diode between the two solenoid contacts, make sure that the
white stripe on the diode is on the negative side of the connection. The
diode acts as a power snubber preventing power spikes from the solenoid
damaging the other electrical components.
  -   connect the resistor to pin B of the transistor then to one of the digital
ports of the arduino, the port depends on the string the solenoid presses
down on, currently string 1 fret 1 is set to port 23 on the arduino.
  - connect pin E of the transistor to the common ground rail.
5. To attach the legs to the holding box insert a M3 nut in to the slot on either
side of the holding box, then push the legs on to the extruding nubs and insert
the M3 bolt through the hole. If the legs do not grip tightly the guitar neck
then padding can be added to the bottom part of the leg.


### Calibrating the system 

Befour using the system the modules may need calibrating to make sure that the modules can interact with the strings properly, this is especially important when first setting up each module.


1. Fretting module
    1. Check the bottom of each finger is above each string, although it is better to have the finger near the bottom of each fret anywhere within the fret will hold the string down although this may be difficult depending on the size of the guitar neck.  
    2. Open the Serial Monitor: `Tools` :arrow_right: `Serial Monitor` end enter a serial code for the fretting finger such <S1F1DV99> and check that the string is held down, this can be repeated for each string but only needs to be done for the top and bottom row of the module. 
    3. if the strings are held down then strum the string to check it is playing the correct note if it is not then adjust the modules height until the correct note is produced


## back

[Robot Guitar Hardware](../CAD files) ---
[Arduino Firmware](../../Firmware) ---
[Python API](../../Python API) ---
[Documents](../../Documents)
