
# Automated Guitar: CAD files

The automation for the guitar has been designed in to three seperate modules.  

## 3D printed parts

You will need to print the following parts in order to build your OpenBot.

1) [Fretting module](fretting module)

2) [Strumming module](struming module)

3) [muting module](muting module)


## 

[Robot Guitar Hardware](../CAD files) ---
[Arduino Firmware](../Firmware) ---
[Python API](Python API) ---
[Documents](../Documents)
