# Automated Guitar: muting module


## 3D printed muting module

<p float="left">
  <img src="../../Documents/pictures and video/muting module.PNG" height="300" />
</p>

You will need to print the following parts in order to build the muting module.

1) muting module [STL](muting module.stl)


Most printers should be able to print the parts, with all parts being able to fit on a 150mmx140mm build plate.

On an Ultimaker 2+, we achieved good results with the following settings:

- layer height: 0.2mm
- wall thickness: 1.5mm
- infill density: 20%
- infill pattern: grid
- print speed 80 mm/s
- supports where required for the muting module.

Parts have been printed using PLA, ABS and PETG all with good results. the print was not affected very much by the print settings. However,  printing slower and with smaller layer height will improve the print quality. Tree supports where found to be easer to remove from prints in post prosessing although normal supports can be used insted. 


## Bill of materials

The robot Guitar relies on readily available hobby electronics. the components can be changed for other makes or types but changing the components will have an effect on each module operates, for some types of guitar changes may be needed to allow the modules to operate as desired. links have been provided for UK suppliers for the components used to create the module, these suppliers are not the only source of the components and they can be bought from other suppliers or locations. 
   You will need the following components:


### General

- 1x Arduino mega was used for this project although any Arduino can be used [UK](https://www.amazon.co.uk/ELEGOO-Controller-ATmega2560-ATMEGA16U2-Compatible/dp/B06XKMZ3T9/ref=sr_1_1_sspa?dchild=1&keywords=Arduino+mega&qid=1614255721&sr=8-1-spons&psc=1&spLa=ZW5jcnlwdGVkUXVhbGlmaWVyPUEzM1FEMENJVzdIVEk4JmVuY3J5cHRlZElkPUEwNTU1OTQ0MjQ4N0QxTTZZNFdDUCZlbmNyeXB0ZWRBZElkPUEwMzI4MTAzMTlYOUw0TUpKUThYMiZ3aWRnZXROYW1lPXNwX2F0ZiZhY3Rpb249Y2xpY2tSZWRpcmVjdCZkb05vdExvZ0NsaWNrPXRydWU=)
- 12V battery is needed to power the modules [UK](https://www.amazon.co.uk/SUNPADOW-Battery-Airplane-Quadcopter-Helicopter/dp/B08Q79M7QB/ref=sr_1_2?dchild=1&keywords=12V+LIPO&qid=1614255912&sr=8-2)

### Muting module add on
- 3D printed mounting frame
- One MG90S servo motor [UK](https://www.amazon.co.uk/Dealikee-Walking-Project-Helicopter-Airplane/dp/B08J78ZWNQ/ref=sr_1_12?dchild=1&keywords=MG90S+Servo+motor&qid=1614264355&sr=8-12)
- MG90S single arm accessory, these come with the servos
- 3D printed muting arm
- padding for the bottom of the muting arm

## Build instructions

#### Muting module
1. Print out both the mounting frame and muting arm and remove supports and
any other stray parts from the printing process
2. Insert servo into the slot in the mounting frame, the slot may be slightly too
small due to warping and shrinkage of the 3D printing, if this occurs the slot
can be made bigger without damaging the structure of the print.
3. Calibrate the servo to make sure it is facing forwards, for the MG90S the front
is 90°, and attach on the single arm accessory that comes with the servo.
4. Attach the felt to the bottom of the muting arm, keeping the same orientation
as when printed.
5. Slide the 3D printed muting arm over the arm accessory already attached to
the servo.
6. To secure the muting one of the mounting screws that came with the MG90S
servo can be inserted through the hole in the back of the muting arm and into
the arm accessory this will prevent the pieces from coming apart.
7. To connect the servo to the Arduino attach the servos data wire(orange wire)
to digital port 9 on the Arduino this port can be changed but if it is then the
new port needs to be set in the Arduino code


### Calibrating the system 

Befour using the system the modules may need calibrating to make sure that the modules can interact with the strings properly, this is especially important when first setting up each module.

1. muting module:
    1. check the muting arm is level across all the strings.
    2. Open the Serial Monitor: `Tools` :arrow_right: `Serial Monitor` end enter <S7SV00BE> this will lower the muting arm.  
        check the arm presses down across all the strings and is not pressing down too hard. 
        check the base of the muting module has not lifted up too far and is straining the connector.
    3. renter <S7SV00BE> in to the serial monitor this will then rase the muting arm
    
 

## Known Issues 
Due to the servos movement magenetic inteferance is produces which can be picked up by some guitars distorting the sound, this inteferance is only seems to effect single coil type pick up, humbucker type pickups are uneffected, this is being current being solved by changing the layout of the module.       


[Robot Guitar Hardware](../CAD files) ---
[Arduino Firmware](../../Firmware) ---
[Python API](../../Python API) ---
[Documents](../../Documents)

