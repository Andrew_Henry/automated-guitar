# Automated Guitar: Picking module


## 3D printed Picking module

<p float="left">
  <img src="../../Documents/pictures and video/Picking module.png" height="300" />
</p>

You will need to print the following parts in order to build the picking module.

1) picking module frame base [STL](picking module frame base.stl)
2) picking module frame [STL](picking module frame.stl)


Most printers should be able to print the parts, with all parts being able to fit on a 150mmx140mm build plate.

On an Ultimaker 2+, we achieved good results with the following settings:

- layer height: 0.2mm
- wall thickness: 1.5mm
- infill density: 20%
- infill pattern: grid
- print speed 80 mm/s
- supports where required for the picking module frame.

Parts have been printed using PLA, ABS and PETG all with good results. The print was not affected very much by the print settings. However,  printing slower and with smaller layer height will improve the print quality. Tree supports where found to be easer to remove from prints in post prosessing although normal supports can be used insted. 


## Bill of materials

The robot Guitar relies on readily available hobby electronics. the components can be changed for other makes or types but changing the components will have an effect on each module operates, for some types of guitar changes may be needed to allow the modules to operate as desired. links have been provided for UK suppliers for the components used to create the module, these suppliers are not the only source of the components and they can be bought from other suppliers or locations. 
   You will need the following components:


### General

- 1x Arduino mega was used for this project although any Arduino can be used [UK](https://www.amazon.co.uk/ELEGOO-Controller-ATmega2560-ATMEGA16U2-Compatible/dp/B06XKMZ3T9/ref=sr_1_1_sspa?dchild=1&keywords=Arduino+mega&qid=1614255721&sr=8-1-spons&psc=1&spLa=ZW5jcnlwdGVkUXVhbGlmaWVyPUEzM1FEMENJVzdIVEk4JmVuY3J5cHRlZElkPUEwNTU1OTQ0MjQ4N0QxTTZZNFdDUCZlbmNyeXB0ZWRBZElkPUEwMzI4MTAzMTlYOUw0TUpKUThYMiZ3aWRnZXROYW1lPXNwX2F0ZiZhY3Rpb249Y2xpY2tSZWRpcmVjdCZkb05vdExvZ0NsaWNrPXRydWU=)
- 12V battery is needed to power the modules [UK](https://www.amazon.co.uk/SUNPADOW-Battery-Airplane-Quadcopter-Helicopter/dp/B08Q79M7QB/ref=sr_1_2?dchild=1&keywords=12V+LIPO&qid=1614255912&sr=8-2)


### picking module

- Six MG90S Servo motor [UK](https://www.amazon.co.uk/Servo-Motor-MG90S-Geared-Helicopter/dp/B07FQMTLD4/ref=sr_1_4_sspa?dchild=1&keywords=MG90S+Servo+motor&qid=1614264154&sr=8-4-spons&psc=1&spLa=ZW5jcnlwdGVkUXVhbGlmaWVyPUEyVkpGUUtWSzAxN1dKJmVuY3J5cHRlZElkPUEwNDUwMzc2MjFEVlo2QzIwTDQ0TyZlbmNyeXB0ZWRBZElkPUEwMTkxMjI2Mk9QVFpOQzI5RUpJUiZ3aWRnZXROYW1lPXNwX2F0ZiZhY3Rpb249Y2xpY2tSZWRpcmVjdCZkb05vdExvZ0NsaWNrPXRydWU=)
- 3D printed mounting frame
- 3D printed mounting plate
- Six 3D printed picking arms
- 4.8 to 6V power supply to power the servos [UK](https://www.amazon.co.uk/Aerzetix-Battery-Holder-Batteries-Storage/dp/B00FXLD7HE/ref=sr_1_5?crid=16OR6ZBROUTMJ&dchild=1&keywords=4x+aa+battery+holder&qid=1614264294&sprefix=4X+AA%2Caps%2C154&sr=8-5)


## Build instructions

### picking module
1. Print out mounting frame, mounting plate and six picking arms and remove
supports and any other stray parts from the printing process
2. Insert a picking arm into the mounting frame with the shorter end of the
picking arm facing away from the servo hole, repeat for all the picking
arms.
3. Calibrate the servo to make sure it is facing forwards, for the MG90S the front
is 90°.
4. Insert servo into the slot in the mounting frame with the shaft of the servo
going in to the hole in the picking arm. The slot or hole may be slightly
too small or misshapen due to warping and shrinkage of the 3D printing, if this
occurs the slot and hole can be made bigger without damaging the structure
of the print. It is recommended to remove all parts from the mounting frame
while changes are made to the slots to prevent any additional damage.
5. Attach the mounting plate around one of the pickups. If the mounting plate
is not a tight fit this is not too much of an issue as the mounting frame helps
keep it together. An adhesive can be used to stick down the mounting plate to
provide a more secure fit without the mounting frame and to make it easer to attach the mounting frame. This can leave resudue on the guitar when removed so is up to the user if they want to do this.
6. Now the picking module can be pushed into the mounting plate, when
attaching the mounting frame to the plate make sure that end of each plectrum
is resting next to each string. 
7. To connect the servo to the arduino, attach the live wire(red wire) to the
common power and ground wire (brown wire) to the common ground which
attaches to one of the arduinos ground ports. Finally connect each of the servo
data wire(orange wire) to digital port 2 through to 7 on the Arduino with
string 1 connected to digital port 2 working the way across, these ports can be
changed but if they are then the new port needs to be set in the Arduino code.


### Calibrating the system 

Befour using the system the modules may need calibrating to make sure that the modules can interact with the strings properly, this is especially important when first setting up each module.

1. picking module:
    1. check the tip of each plectrum is next to the string, if all the plectrums are above the strings then the mounting frame may not be pushed in fully.
    2. Open the Serial Monitor: `Tools` :arrow_right: `Serial Monitor` end enter <S6SV00BE> this should strum the top E string on the guitar, 
        if it strums a different string then check the servo is connected in the correct location. 
        if it does not strum a string look to see if the plectrum is too high if it is then the legs of the mounting frame can be shortened or the string can be lifted from the mounting on the guitar.
        if the plectrum is too low and hits the string but does not pluck it then the frame needs lifting up or the string can be lowered from the mounting on the guitar
    3. repeat checking each plectrum by entering there serial codes <SXSV00BE> where x is the string number, and adjust the guitar as needed.
    4. if the strings have been lowered then the guitar may need retuning.
 


## Known Issues 
Due to the servos movement magenetic inteferance is produces which can be picked up by some guitars distorting the sound, this inteferance is only seems to effect single coil type pick up, humbucker type pickups are uneffected, this is being current being solved by changing the layout of the struming module.       


[Robot Guitar Hardware](../CAD files/README.md) ---
[Arduino Firmware](../../Firmware/README.md) ---
[Serial Script](../../Serial Script/README.md) ---
[Documents](../../Documents)
